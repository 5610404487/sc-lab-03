import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class SoftwareTesting {

    class ListenerMgr implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				System.exit(0);
			}   
	 }
	
	public static void main(String[] args) {
         new SoftwareTesting();
	}
	public SoftwareTesting() {
		frame = new SoftwareFrame();
        frame.pack();
        frame.setVisible(true);
        frame.setSize(400,400);
        list = new ListenerMgr();
        frame.setListener(list);
        setTestCase();
	}
    public void setTestCase() {
    	String str1 = JOptionPane.showInputDialog("Please Enter Word 1");
    	String str2 = JOptionPane.showInputDialog("Please Enter Word 2");

    	int num = 3;
    	PartWord gram1 = new PartWord();
    	ArrayList gengram1 = gram1.generateNgram(str1);
    	ArrayList ngram1 = gram1.generateNgram2(num);
    	frame.setResult("N = 3");
    	frame.extendResult("Word :: "+gengram1.toString());
    	frame.extendResult("Ngram :: "+ngram1.toString());

		PartWord gram2 = new PartWord();
    	ArrayList gengram2 = gram2.generateNgram(str2);
    	ArrayList ngram2 = gram2.generateNgram2(num);
    	frame.extendResult("Word :: "+gengram2.toString());
    	frame.extendResult("Ngram :: "+ngram2.toString());
    }
    ActionListener list;
    SoftwareFrame frame;
}

import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class PartWord {
private ArrayList<String> list = new ArrayList<String>();
	
	public ArrayList generateNgram(String str){
		
		StringTokenizer token = new StringTokenizer(str);
		ArrayList<String> list = new ArrayList<String>();
		while (token.hasMoreTokens()) {
	    	 list.add(token.nextToken());
	     }
		this.list=list;
		return list;
	}
	public ArrayList generateNgram2(int num){
		
		String n = ("");
		
		for(String i : list){
			n += i;
		}
		ArrayList<String> lists = new ArrayList<String>();
		for(int i = 0; i <= n.length()-num; i++){
			lists.add(n.substring(i,i+num));
		}
		return lists;
	}

}
